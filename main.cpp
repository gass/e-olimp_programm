#include <iostream>
#include <list>
#include <queue>

using namespace std;

class Graph {
public:
    Graph ();
    void BFS ();
private:
	list <int> *incid_list;
    char *weights;
    int N;
    int M;
    int K;
};


Graph::Graph() {
    cin>>M;
    cin>>N;
    K=N*M;
    incid_list = new list<int> [K];
    for (int i=0; i<K; i++) {
        incid_list[i].push_back(i);
    }
    weights= new char [K];
    for (int i=0; i<K; i++) {
        cin>>weights[i];
    }
    int k=2;
    int m=1;
    for (int i=0; i<K; i++) {
        if (i>0 && i<N-1) {
            incid_list[i].push_back(i+1);
            incid_list[i].push_back(i+N);
            incid_list[i].push_back(i-1);
        } else {
            if (i==N*k-1 && i!=K-1) {
                incid_list[i].push_back(i-1);
                incid_list[i].push_back(i+N);
                incid_list[i].push_back(i-N);
                k++;
            } else {
                if (i==N*m && i!=(N-1)*m && i!=N*(M-1)) {
                    incid_list[i].push_back(i+1);
                    incid_list[i].push_back(i+N);
                    incid_list[i].push_back(i-N);
                    m++;
                } else {
                    if (i>(M-1)*N && i<K-1) {
                        incid_list[i].push_back(i+1);
                        incid_list[i].push_back(i-1);
                        incid_list[i].push_back(i-N);
                    } else {
                        if (i==N-1) {
                            incid_list[i].push_back(i-1);
                            incid_list[i].push_back(i+N);
                        } else {
                            if (i==K-1) {
                                incid_list[i].push_back(i-1);
                                incid_list[i].push_back(i-N);
                            } else {
                                if (i==0) {
                                    incid_list[i].push_back(i+1);
                                    incid_list[i].push_back(i+N);
                                } else {
                                    if (i==N*(M-1)) {
                                        incid_list[i].push_back(i+1);
                                        incid_list[i].push_back(i-N);
                                    } else {
                                        incid_list[i].push_back(i-1);
                                        incid_list[i].push_back(i-N);
                                        incid_list[i].push_back(i+1);
                                        incid_list[i].push_back(i+N);
                                    }
                                } 
                            }
                        }
                    }
                }
            }
        }
    } 
}

void Graph::BFS () {
    int v;
    int *color = new int [K];
    queue<int> Q;
    list <int>::iterator it;

    for (int i=0; i<K; i++) {
        color[i]=-1;
    }
    
    int parts=0;

    for (int i=0; i<K; i++) {
        if (weights[i]=='#' && color[i]==-1) {
            color[i]=0;
            Q.push(i);
            parts++; 
            while (!Q.empty()) {
                v=Q.front();
                Q.pop();
                for (it=incid_list[v].begin(); it!=incid_list[v].end(); it++) {
                    if(color[*it]==-1 && weights[*it]=='#') {
                        color[*it]=0;
                        Q.push(*it);
                    } 
                }
                color[v]=1;
            }
        }
    }
    cout<<parts<<endl;
}

int main () {
    Graph G;
    G.BFS();
    return 0;
}
